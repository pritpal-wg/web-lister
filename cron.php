<?php

//date_default_timezone_set('Asia/Kolkata');
require_once rtrim(dirname(dirname(dirname(__DIR__))), '/\\') . '/wp-config.php';
require_once rtrim(__DIR__, '/\\') . '/wl-config.php';

$campaigns = wl_get_campaign();
foreach ($campaigns as $campaign) {
    //skip if campaign is currently processing in another instance of cron
    if ($campaign->start_datetime != '0000-00-00 00:00:00' && $campaign->end_datetime == '0000-00-00 00:00:00')
        continue;

    //get campaign id
    $campaign_id = $campaign->id;

    //skip if csv url and template are not there
    {
        $csv_url = wl_get_campaign_csv_url($campaign_id);
        $campaign_template = wl_get_campaign_template($campaign_id);
        //skip campaign if no csv url or template found
        if (!$csv_url || !$campaign_template)
            continue;
    }

    //skip if invalid columns and associated variables
    {
        $cols_vars = unserialize($campaign_template->cols_vars);
        if (!is_array($cols_vars))
            continue;
        $vars_cols = array_flip($cols_vars);
    }

    //get current time value
    $now_time = strtotime(date('Y-m-d H:i:s'));

    //check if already processed
    {
        $prev_sch = $wpdb->get_row("SELECT * FROM `" . WL_TABLE_CAMPAIGN_SCHEDULE . "` WHERE campaign_id = '$campaign_id'");
        if ($prev_sch) {
            $prev_sch_id = $prev_sch->id;

            //skip if upload type file
            if ($prev_sch->reimport_datetime == '0000-00-00 00:00:00')
                continue;
        }
    }

    //check if campaign can be executed for first time
    if ($campaign_template->first_publish_unit && $campaign_template->first_publish_value) {
        $campaign_process_time = strtotime($campaign->date_added . " + $campaign_template->first_publish_value $campaign_template->first_publish_unit");
    }

    //check if campaign can be executed for auto publish
    if (isset($prev_sch_id) && $campaign_template->auto_publish_unit && $campaign_template->auto_publish_value) {
        $campaign_process_time = strtotime($campaign->end_datetime . " + $campaign_template->auto_publish_value $campaign_template->auto_publish_unit");
    }

    //skip if campaign can't be executed now
    if (isset($campaign_process_time) && $now_time < $campaign_process_time)
        continue;


    //check if old posts needs deletion
    if ($campaign->delete_old_posts) {
        $old_posts_query = new WP_Query(array(
            'post_type' => 'post',
            'post_status' => 'any',
            'posts_per_page' => -1,
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'key' => '_wl_post',
                    'value' => 1,
                ),
                array(
                    'key' => '_wl_campaign_id',
                    'value' => $campaign_id,
                ),
            ),
        ));
        if ($old_posts_query->have_posts()) {
            //delete posts here
            $old_posts_ids = $old_posts_query->posts;
            $in_posts = implode(",", $old_posts_ids);
            $query = "DELETE FROM `$wpdb->posts` WHERE `ID` IN ($in_posts)";
            $wpdb->query($query);
            $query = "DELETE FROM `$wpdb->postmeta` WHERE `post_id` IN ($in_posts)";
            $wpdb->query($query);
        }
        wp_reset_query();
    }

    //get columns from csv file
    {
        $handle = fopen($csv_url, 'r');
        $columns = fgetcsv($handle);
    }

    //skip if columns from csv and template are not matched
    if (!empty(array_diff($vars_cols, $columns)) || !empty(array_diff($columns, $vars_cols))) {
        fclose($handle);
        continue;
    }

    //--------------everything is ok, start import--------------------------
    //reset the campaign start and end date
//    $wpdb->update(WL_TABLE_CAMPAIGNS, array('start_datetime' => date('Y-m-d H:i:s'), 'end_datetime' => ''), array('id' => $campaign_id));
    //get template for post content
    $post_content_template = unserialize($campaign_template->post_content);

    //get valid filters(skip if any of the field, unit, value is not found for any filter)
    $filters = array_filter(unserialize($campaign_template->filters), function($e) {
        return ($e['field'] && $e['unit'] && $e['value']) ? $e : '';
    });

    //number of columns for table in post content
    $num_table_cols = count($post_content_template);

    //number of rows used for a specific template
    {
        $wl_import_post_records = get_option('wl_import_post_records', 10);
        if (!is_numeric($wl_import_post_records))
            $wl_import_post_records = 10; //default value 10
    }

    //initialise the number of posts and lines for campaign
    {
        $posts_added = 0;
        $tot_lines = 0;
    }

    //read till end of file
    while (!feof($handle)) {
        //make formatted array of line received from csv
        {
            $i = 0;
            $line = array_map(function($val) {
                global $cols_vars, $columns, $i;
                $col = $columns[$i];
                $var = $cols_vars[$columns[$i]];
                $arr = array(
                    'col' => $col,
                    'var' => $var,
                    'val' => trim($val),
                );
                $i++;
                return $arr;
            }, fgetcsv($handle));
        }

        //apply filters and skip if any one is failed
        {
            $skip = FALSE;
            foreach ($filters as $filter) {
                if ($filter['unit'] == 'eq' && (wl_fill_post_var_vals($filter['field']) != wl_fill_post_var_vals($filter['value'])))
                    $skip = TRUE;
                elseif ($filter['unit'] == 'lg' && (wl_fill_post_var_vals($filter['field']) < wl_fill_post_var_vals($filter['value'])))
                    $skip = TRUE;
                elseif ($filter['unit'] == 'sm' && (wl_fill_post_var_vals($filter['field']) > wl_fill_post_var_vals($filter['value'])))
                    $skip = TRUE;
                elseif ($filter['unit'] == 'df' && (wl_fill_post_var_vals($filter['field']) == wl_fill_post_var_vals($filter['value'])))
                    $skip = TRUE;
            }
            if ($skip === TRUE)
                continue;
        }

        //check if post is initiated
        if (!isset($new_post)) {
            $new_post = array();
            $new_post['post_title'] = wl_fill_post_var_vals($campaign_template->post_title);
            $new_post['post_status'] = $campaign_template->post_status;
            $new_post['ping_status'] = $campaign_template->trackbacks ? 'open' : 'closed';
            $new_post['comment_status'] = $campaign_template->comments ? 'open' : 'closed';
            $new_post['post_category'] = array($campaign_template->category_id);
            //post category in case of the campaign categories
            if ($campaign_template->use_camp_categories == 1 && $campaign_template->camp_categories) {
                if (!function_exists('wp_create_category'))
                    require_once ABSPATH . 'wp-admin/includes/taxonomy.php';
                $camp_categories = unserialize($campaign_template->camp_categories);
                $parent_category = $campaign_template->camp_parent_category;
                foreach ($camp_categories as $camp_category) {
                    $cat_name = wl_fill_post_var_vals($camp_category);
                    $parent_category = wp_create_category($cat_name, $parent_category);
                }
                if ($parent_category) {
                    $new_post['post_category'] = array($parent_category);
                }
            }
            $new_post['tags_input'] = wl_fill_post_var_vals($campaign_template->tags);
            //post_content
            {
                $rows_added = 0;
                $new_post['post_content'] = ($campaign_template->post_prefix ? $campaign_template->post_prefix : '') . '<table border="1" style="width: 100%;padding: 1px;margin:0"><thead><tr style="padding: 1px;margin:0">';
                for ($i = 1; $i <= $num_table_cols; $i++) {
                    $heading = wl_fill_post_var_vals($post_content_template[$i]['heading']);
                    $new_post['post_content'] .= '<th style="padding: 1px;margin:0">' . $heading . '</th>';
                }
                $new_post['post_content'] .= '</tr></thead><tbody>';
            }
        }
        $new_post['post_content'] .= "<tr>";
        for ($i = 1; $i <= $num_table_cols; $i++) {
            $content = wl_fill_post_var_vals($post_content_template[$i]['text']);
            $new_post['post_content'] .= '<td style="padding: 1px;margin:0">' . $content . '</td>';
        }
        $new_post['post_content'] .= "</tr>";
        $rows_added++;
        if ($rows_added == $wl_import_post_records) {
            $new_post['post_content'] .= "</tbody></table>" . ($campaign_template->post_postfix ? $campaign_template->post_postfix : '');
            if ($new_post_id = wp_insert_post($new_post)) {
                $posts_added++;
                update_post_meta($new_post_id, '_wl_post', 1);
                update_post_meta($new_post_id, '_wl_campaign_id', $campaign_id);
            }
            unset($new_post);
        }
        $tot_lines++;
    }
    if (isset($new_post)) {
        $new_post['post_content'] .= "</tbody></table>" . ($campaign_template->post_postfix ? $campaign_template->post_postfix : '');
        if ($new_post_id = wp_insert_post($new_post)) {
            $posts_added++;
            update_post_meta($new_post_id, '_wl_post', 1);
            update_post_meta($new_post_id, '_wl_campaign_id', $campaign_id);
        }
        unset($new_post);
    }
    fclose($handle);

    //set the end date for the campaign
    {
        $wpdb->update(WL_TABLE_CAMPAIGNS, array('end_datetime' => date('Y-m-d H:i:s')), array('id' => $campaign_id));
    }

    //update the total lines and articles values for campaign
    $wpdb->update(WL_TABLE_CAMPAIGNS, array('tot_lines' => $tot_lines, 'tot_articles' => $posts_added), array('id' => $campaign_id));

    //add campaign to schedule table and set schedule info if required
    {
        //initialize data for insertion or updation
        $data = array('campaign_id' => $campaign_id);

        //if reimport is required
        if ($campaign->reimport_value && $campaign->reimport_unit) {
            $data['reimport_datetime'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+ $campaign->reimport_value $campaign->reimport_unit"));
        }

        if (isset($prev_sch_id)) {
            $wpdb->update(WL_TABLE_CAMPAIGN_SCHEDULE, $data, array('id' => $prev_sch_id));
        } else {
            $wpdb->insert(WL_TABLE_CAMPAIGN_SCHEDULE, $data);
        }
    }
    echo "Camapign - <b>$campaign->title</b><br />Posts Added: " . $posts_added;
    echo "<hr />";
}