<?php

/**
 * Plugin Name:       Web Lister
 * Description:       Web Lister Plugin
 * Version:           1.0.0
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       web-lister
 * Domain Path:       /languages
 */
require_once rtrim(__DIR__, '/\\') . '/wl-config.php';

function activate_web_lister() {
    require_once WL_DIR_PATH . 'includes/class-web-lister-activator.php';
    WebLister_Activator::activate();
}

register_activation_hook(__FILE__, 'activate_web_lister');

function deactivate_web_lister() {
    require_once WL_DIR_PATH . 'includes/class-web-lister-deactivator.php';
    WebLister_Deactivator::deactivate();
}

register_deactivation_hook(__FILE__, 'deactivate_web_lister');

require_once WL_DIR_PATH . 'includes/class-web-lister.php';

function run_web_lister() {

    $plugin = new WebLister();
    $plugin->run();
}

run_web_lister();
