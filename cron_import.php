<?php

require_once rtrim(dirname(dirname(dirname(__DIR__))), '/\\') . '/wp-config.php';
require_once rtrim(__DIR__, '/\\') . '/wl-config.php';

//get campaigns which require reimport
$query = "SELECT * FROM `" . WL_TABLE_CAMPAIGNS . "` WHERE id IN (SELECT DISTINCT campaign_id FROM `" . WL_TABLE_CAMPAIGN_SCHEDULE . "`) AND reimport_value != '' AND reimport_unit != ''";
$campaigns = $wpdb->get_results($query);

//get current time value
$now_time = strtotime(date('Y-m-d H:i:s'));

//set the imported campaigns to 0
$imported = 0;

//loop through campaigns
foreach ($campaigns as $campaign) {
    //get campaign id
    $campaign_id = $campaign->id;

    //get previous scheduled info
    {
        $prev_sch = $wpdb->get_row("SELECT * FROM `" . WL_TABLE_CAMPAIGN_SCHEDULE . "` WHERE campaign_id = '$campaign_id'");
        $prev_sch_id = $prev_sch->id;
    }

    //campaign applicable reimport time
    $reimport_time = strtotime($prev_sch->reimport_datetime);

    //skip if current time is less than the reiport date
    if ($now_time < $reimport_time)
        continue;

    // ----------- The CSV File can be Reimported --------------- //
    //import csv url
    {
        $import_url = $campaign->import_file_url;
        //skip if invalid file
        if (filter_var($import_url, FILTER_VALIDATE_URL) === FALSE)
            continue;

        //skip if not csv file
        {
            $pathinfo = pathinfo($import_url);
            if ($pathinfo['extension'] !== 'csv')
                continue;
        }
    }

    //upload file path
    {
        $upload_file_name = $campaign->upload_file_name;
        $import_local_path = WL_UPLOAD_DIR . $upload_file_name;
    }

    //get the file content
    $file_content = file_get_contents($import_url);

    //save online data to local file
    {
        $handle = fopen($import_local_path, 'w');
        fwrite($handle, $file_content);
        fclose($handle);
    }

    //set new import values
    {
        $data = array(
            'campaign_id' => $campaign_id,
            'reimport_datetime' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+ $campaign->reimport_value $campaign->reimport_unit")),
        );
        $wpdb->update(WL_TABLE_CAMPAIGN_SCHEDULE, $data, array('id' => $prev_sch_id));
    }

    //increment number of imported campaigns
    $imported++;
}
echo "Total Imported Campaigns: $imported";