<?php

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

@ini_set('auto_detect_line_endings', TRUE);
global $wpdb;
//defines
{
    //path and url
    define('WL_DIR_PATH', untrailingslashit(__DIR__) . '/');
    define('WL_DIR_URL', untrailingslashit(plugin_dir_url(__FILE__)) . '/');
    require_once WL_DIR_PATH . 'includes/web-lister-functions.php';

    //domain & sttings group
    define('WL_DOMAIN', 'web-lister');
    define('WL_SETTINGS_GROUP', 'wl_settings_group');

    //table_prefix
    define('WL_PREFIX', $wpdb->prefix . 'wl_');

    //tables for plugin
    {
        define('WL_TABLE_CAMPAIGNS', WL_PREFIX . 'campaigns');
        define('WL_TABLE_CAMPAIGN_TEMPLATES', WL_PREFIX . 'campaign_templates');
        define('WL_TABLE_CAMPAIGN_SCHEDULE', WL_PREFIX . 'campaign_schedule');
    }

    //campaign upload directory and path
    {
        $wl_upload_folder_name = 'web-lister';
        $wl_upload_dir = trailingslashit(wp_upload_dir()['basedir']) . "$wl_upload_folder_name/";
        if (!is_dir($wl_upload_dir))
            @mkdir($wl_upload_dir);
        if (!is_dir($wl_upload_dir))
            user_error("Unable to create directory <b>" . $wl_upload_dir . "</b><hr />Please make the folder writable", E_ERROR);
        $wl_upload_dir_url = trailingslashit(wp_upload_dir()['baseurl']) . "$wl_upload_folder_name/";
        define('WL_UPLOAD_DIR', $wl_upload_dir);
        define('WL_UPLOAD_DIR_URL', $wl_upload_dir_url);

        //reset used variables
        unset($wl_upload_folder_name);
        unset($wl_upload_dir);
        unset($wl_upload_dir_url);
    }
}