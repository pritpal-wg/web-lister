<?php

class WebLister_Admin {

    private $plugin_name;
    private $version;

    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    public function enqueue_styles() {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/web-lister-admin.css', array(), $this->version, 'all');
    }

    public function enqueue_scripts() {
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/web-lister-admin.js', array('jquery'), $this->version, false);
    }

    public function make_admin_menu_active() {
        if (function_exists('get_current_screen')) {
            $screen = get_current_screen();
            $allowed_ids = array(
                'edit-post',
                'edit-category',
                'admin_page_weblister_campaign_settings'
            );
//            pr($screen);die;
            ?>
            <?php if (in_array($screen->id, $allowed_ids)) { ?>
                <script>
                    if (jQuery) {
                        (function ($) {
                            $(document).ready(function () {
                                var li = $('li#toplevel_page_weblister');
                                var a = $('li#toplevel_page_weblister>a');
                                li.removeClass('wp-not-current-submenu').addClass('wp-has-current-submenu');
                                a.removeClass('wp-not-current-submenu').addClass('wp-has-current-submenu');
                <?php if ($screen->id === 'edit-post') { ?>
                                    li.find('.wp-submenu li a[href$="weblister_articles"]').addClass('current').parent().addClass('current');
                <?php } ?>
                <?php if ($screen->id === 'edit-category') { ?>
                                    li.find('.wp-submenu li a[href$="weblister_article_categories"]').addClass('current').parent().addClass('current');
                <?php } ?>
                            });
                        })(jQuery);
                    }
                </script>
            <?php } ?>
        <?php } ?>
        <?php
    }

    public function register_settings() {
        register_setting(WL_SETTINGS_GROUP, 'wl_import_lines');
        register_setting(WL_SETTINGS_GROUP, 'wl_import_post_records');
//        register_setting('myoption-group', 'some_other_option');
//        register_setting('myoption-group', 'option_etc');
    }

    public function admin_menu() {
        add_menu_page('Web Lister', 'Web Lister', 'manage_options', 'weblister', '', '', 100);
        //add submenu pages
        {
            add_submenu_page('weblister', 'Campaign List', 'Campaign List', 'manage_options', 'weblister', array($this, 'weblister_callback'));
            add_submenu_page('weblister', 'New Campaign', 'New Campaign', 'manage_options', 'weblister_campaign', array($this, 'weblister_campaign_callback'));
            add_submenu_page('weblister', 'Settings', 'Settings', 'manage_options', 'weblister_settings', array($this, 'weblister_settings_callback'));
            add_submenu_page('weblister', 'Articles', 'Articles', 'manage_options', 'weblister_articles', array($this, 'weblister_articles_callback'));
            add_submenu_page('weblister', 'Article Categories', 'Article Categories', 'manage_options', 'weblister_article_categories', array($this, 'weblister_article_categories_callback'));
        }

        //pages that are used in the process
        {
            $extra_pages = array(
                //self_public_function_to_hook => menu-slug
                'weblister_campaign_settings_callback' => 'weblister_campaign_settings'
            );
            if (!empty($extra_pages)) {
                global $_registered_pages;
                foreach ($extra_pages as $hook_function => $slug) {
                    if ($slug) {
                        $hookname = get_plugin_page_hookname($slug, '');
                        if (!empty($hookname)) {
                            add_action($hookname, array($this, $hook_function));
                            $_registered_pages[$hookname] = true;
                        }
                    }
                }
            }
        }
    }

    //callbacks for admin pages
    public function weblister_callback() {
        include WL_DIR_PATH . 'admin/partials/campaign-list.php';
    }

    public function weblister_campaign_callback() {
        include WL_DIR_PATH . 'admin/partials/campaign-single.php';
    }

    public function weblister_settings_callback() {
        include WL_DIR_PATH . 'admin/partials/settings.php';
    }

    public function weblister_articles_callback() {
        $redirect = untrailingslashit(get_site_url()) . '/wp-admin/edit.php';
        echo '<script>window.location = "' . $redirect . '";</script>';
        die;
    }

    public function weblister_article_categories_callback() {
        $redirect = untrailingslashit(get_site_url()) . '/wp-admin/edit-tags.php?taxonomy=category';
        echo '<script>window.location = "' . $redirect . '";</script>';
        die;
    }

    //callbacks for extra pages
    public function weblister_campaign_settings_callback() {
        include WL_DIR_PATH . 'admin/partials/campaign-settings.php';
    }

}
