<?php
$id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0 ? $_GET['id'] : 0;
$redirect = admin_url('admin.php?page=');
if (!$id || !$campaign = wl_get_campaign($id)) {
    $redirect .= "weblister";
    echo '<script>window.location = "' . $redirect . '";</script>';
    die;
}
if ($campaign->upload_file_name) {
    $csv_url = WL_UPLOAD_DIR_URL . $campaign->upload_file_name;
    $local_file = TRUE;
} elseif ($campaign->import_file_url) {
    $csv_url = $campaign->import_file_url;
    $local_file = FALSE;
} else {
    $redirect .= "weblister_campaign&action=import_csv&id=$id";
    echo '<script>window.location = "' . $redirect . '";</script>';
    die;
}

//get global db object
global $wpdb;
$campaign_template_id = 0;
if ($campaign_template = $wpdb->get_row("SELECT * FROM `" . WL_TABLE_CAMPAIGN_TEMPLATES . "` WHERE campaign_id=$id"))
    $campaign_template_id = $campaign_template->id;

//get columns
{
    $handle = fopen($csv_url, 'r');
    $columns = fgetcsv($handle);
    fclose($handle);
}

//campaign name
$camp_name = $campaign->title;

//handle post submit
if (isset($_POST['sub'])) {
    $data = array();
    $data['campaign_id'] = $id;
    $data['category_id'] = $_POST['category_id'];
    if (isset($_POST['wl_camp_parent_category'])) {
        $data['use_camp_categories'] = 1;
        $data['camp_parent_category'] = trim($_POST['wl_camp_parent_category']) ? trim($_POST['wl_camp_parent_category']) : 0;
        $data['camp_categories'] = serialize(array_filter($_POST['wl_camp_categories']));
    } else {
        $data['use_camp_categories'] = 0;
        $data['camp_parent_category'] = 0;
        $data['camp_categories'] = '';
    }
    $data['cols_vars'] = serialize($_POST['cols_vars']);
    $data['filters'] = serialize($_POST['filters']);
    $data['post_title'] = $_POST['post_title'];
    $data['post_content'] = serialize($_POST['post_content']);
    $data['post_prefix'] = $_POST['post_prefix'];
    $data['post_postfix'] = $_POST['post_postfix'];
    $data['tags'] = $_POST['tags'];
    $data['comments'] = $_POST['comments'];
    $data['trackbacks'] = $_POST['trackbacks'];
    $data['meta_title'] = $_POST['meta_title'];
    $data['meta_keywords'] = $_POST['meta_keywords'];
    $data['meta_description'] = $_POST['meta_description'];
    $data['first_publish_value'] = $_POST['first_publish_value'];
    $data['first_publish_unit'] = $_POST['first_publish_unit'];
    $data['post_status'] = $_POST['post_status'];
    $data['auto_publish_value'] = $_POST['auto_publish_value'];
    $data['auto_publish_unit'] = $_POST['auto_publish_unit'];
    $data['random_times'] = $_POST['random_times'];
    if ($campaign_template_id) {
        $wpdb->update(WL_TABLE_CAMPAIGN_TEMPLATES, $data, array('id' => $campaign_template_id));
    } else {
        $wpdb->insert(WL_TABLE_CAMPAIGN_TEMPLATES, $data);
    }
    $redirect .= "weblister";
    echo '<script>window.location = "' . $redirect . '";</script>';
    die;
}
?>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<div class="wrap">
    <h1><?php _e('Edit Campaign', WL_DOMAIN) ?></h1>
    <form method="post" enctype="multipart/form-data" action="<?php echo trailingslashit(get_site_url()) . 'wp-admin/admin.php?page=weblister_campaign_settings' . ($id ? "&id=$id" : '') ?>">
        <input type="hidden" id="campaign_id" value="<?php echo $id ?>" />
        <?php if ($campaign_template_id) { ?>
            <input type="hidden" id="campaign_template_id" value="<?php echo $campaign_template_id ?>" />
        <?php } ?>
        <div style="float: right;width: 20em;border: solid 1px;position: absolute;top:40px;right:30px;padding: 10px;padding-top: 0;max-height: 320px;overflow: auto">
            <h4>CSV Fields</h4>
            The plugin detected the following CSV fields and assigned its variables:
            <br />
            <br />
            <table style="width: 100%">
                <thead>
                    <tr>
                        <th style="border-bottom: dashed 1px">CSV Field</th>
                        <th style="border-bottom: dashed 1px">Variable</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    ?>
                    <?php foreach ($columns as $column) { ?>
                        <?php
                        $col_var = '{wl:' . $i++ . '}';
                        ?>
                        <tr>
                            <td style="text-align: center;width: 55%"><?php echo $column ?></td>
                            <td style="text-align: center">
                                <span id="col_var_<?php echo $i ?>" onclick="selectText('col_var_<?php echo $i ?>')"><?php echo $col_var ?></span>
                                <input type="hidden" name="cols_vars[<?php echo $column; ?>]" value="<?php echo $col_var ?>" />
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="campaign_name"><?php _e('Campaign Name', WL_DOMAIN) ?></label>
                    </th>
                    <td><input type="text" class="regular-text" value="<?php echo $camp_name ?>" readonly /></td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Category', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        $prev_cat_id = 0;
                        if ($campaign_template) {
                            $prev_cat_id = $campaign_template->category_id;
                            if ($prev_cat_id) {
                                $prev_cat = get_category($prev_cat_id);
                            }
                        }
                        ?>
                        <?php if (isset($prev_cat)) { ?>
                            <div>Previously Selected Category: <b><?php echo $prev_cat->name ?></b></div>
                        <?php } ?>
                        <input type="hidden" name="category_id" id="category_id" value="<?php echo $prev_cat_id ?>" />
                        <div id="load_categories_container" style="min-height: 40px;max-height: 200px;overflow: auto;border: dashed 1px #ccc;padding: 10px;padding-top: 0;width: 30em"></div>
                        <div style="width: 30em;text-align: center">
                            <h3>-- OR --</h3>
                        </div>
                        <div id="add_categories_container" style="max-height: 210px;overflow: auto;border: dashed 1px #ccc;padding: 10px;width: 30em"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Filters', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        if ($campaign_template) {
                            $prev_filters = $campaign_template->filters ? unserialize($campaign_template->filters) : array();
                            $temp = array();
                            foreach ($prev_filters as $prev_filter)
                                if ($prev_filter['field'] && $prev_filter['unit'] && $prev_filter['value'])
                                    $temp[] = $prev_filter;
                            $prev_filters = !empty($temp) ? $temp : null;
                        }
                        ?>
                        <div id="filters_div" style="min-height: 40px;max-height: 200px;overflow: auto;border: dashed 1px #ccc;padding: 10px;padding-top: 0;margin-bottom: 10px;width: 30em" data-init_filters="<?php echo $prev_filters ? 0 : 1; ?>">
                            <?php
                            if ($prev_filters) {
                                foreach ($prev_filters as $k => $prev_filter) {
                                    $prev_filter_unit = $prev_filter['unit'];
                                    ob_start();
                                    ?>
                                    <div style="margin-top: 10px" data-for="filter">
                                        <input type="text" style="width: 60px" name="filters[<?php echo $k ?>][field]" placeholder="Field" value="<?php echo $prev_filter['field'] ?>" />
                                        <select name="filters[<?php echo $k ?>][unit]">
                                            <option value="" hidden>Unit</option>
                                            <option value="eq"<?php echo $prev_filter_unit === 'eq' ? ' selected' : '' ?>>= (<?php _e('equal', WL_DOMAIN) ?>)</option>
                                            <option value="lg"<?php echo $prev_filter_unit === 'lg' ? ' selected' : '' ?>>&gt; (<?php _e('larger', WL_DOMAIN) ?>)</option>
                                            <option value="sm"<?php echo $prev_filter_unit === 'sm' ? ' selected' : '' ?>>&lt; (<?php _e('smaller', WL_DOMAIN) ?>)</option>
                                            <option value="df"<?php echo $prev_filter_unit === 'df' ? ' selected' : '' ?>>&lt;&gt; (<?php _e('different', WL_DOMAIN) ?>)</option>
                                        </select>
                                        <input type="text" style="width: 150px" name="filters[<?php echo $k ?>][value]" placeholder="Value" value="<?php echo $prev_filter['value'] ?>" />
                                    </div>
                                    <?php
                                    echo ob_get_clean();
                                }
                            }
                            ?>
                        </div>
                        <a href="javascript:;" id="add_filter">Add Filter</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Post Title', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        $post_title = '';
                        if ($campaign_template) {
                            $post_title = $campaign_template->post_title;
                        }
                        ?>
                        <input type="text" class="regular-text" name="post_title" value="<?php echo $post_title ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Post Content', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        $post_prefix = '';
                        $post_postfix = '';
                        if ($campaign_template) {
                            $post_prefix = $campaign_template->post_prefix;
                            $post_postfix = $campaign_template->post_postfix;
                        }
                        ?>
                        <b>Post Content Prefix</b><br />
                        <textarea name="post_prefix" id="post_prefix" style="width: 100%"><?php echo $post_prefix ?></textarea>
                        <hr />
                        <?php
                        $content_table_columns = 4;
                        $post_content = null;
                        if ($campaign_template) {
                            $post_content = $campaign_template->post_content ? unserialize($campaign_template->post_content) : null;
                            if (!empty($post_content)) {
                                $content_table_columns = count($post_content);
                            }
                        }
                        ?>
                        Number of Columns in Table: <input id="content_table_columns" type="number" min="1" max="15" value="<?php echo $content_table_columns ?>" style="width: 60px" />
                        <p class="description">Changing the number of columns will reset the data in Table</p>
                        <hr />
                        <div id="content_table_container" data-init_show_table="<?php echo $post_content ? 0 : 1 ?>">
                            <?php
                            if ($post_content) {
                                $cols = $content_table_columns;
                                ?>
                                <table border="1" style="width: 100%" id="content_table">
                                    <thead>
                                        <tr>
                                            <?php for ($i = 0; $i < $cols; $i++) { ?>
                                                <th><input type="text" placeholder="Heading Text" name="post_content[<?php echo $i + 1 ?>][heading]" style="width: 100%" value="<?php echo $post_content[$i + 1]['heading'] ?>" /></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php for ($i = 0; $i < $cols; $i++) { ?>
                                                <td><input type="text" name="post_content[<?php echo $i + 1 ?>][text]" style="width: 100%" placeholder="Content" value="<?php echo $post_content[$i + 1]['text'] ?>" /></td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                                <style>
                                    #content_table,
                                    #content_table tr,
                                    #content_table tr th,
                                    #content_table tr td
                                    {
                                        padding: 1px;
                                        margin:0;
                                    }
                                </style>
                                <?php
                            }
                            ?>
                        </div>
                        <hr />
                        <b>Post Content Postfix</b><br />
                        <textarea style="width: 100%" id="post_postfix" name="post_postfix"><?php echo $post_postfix ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Tags', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        $tags = '';
                        if ($campaign_template)
                            $tags = $campaign_template->tags;
                        ?>
                        <input type="text" class="regular-text" name="tags" value="<?php echo $tags ?>" />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Comments & Trackbacks', WL_DOMAIN) ?></th>
                    <td>
                        <?php
                        $comments = 1;
                        $trackbacks = 1;
                        if ($campaign_template) {
                            $comments = $campaign_template->comments;
                            $trackbacks = $campaign_template->trackbacks;
                        }
                        ?>
                        <fieldset><legend class="screen-reader-text"><span>Comments & Trackbacks</span></legend>
                            <label><input type="radio" name="comments" value="1" checked="checked"> Allow Comments</label>
                            <label><input type="radio" name="comments" value="0"<?php echo!$comments ? ' checked' : '' ?>> Disable Comments</label><br>
                            <label><input type="radio" name="trackbacks" value="1" checked="checked"> Allow Trackbacks</label>
                            <label><input type="radio" name="trackbacks" value="0"<?php echo!$trackbacks ? ' checked' : '' ?>> Disable Trackbacks</label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row" colspan="2"><?php _e('All-in-one SEO Fields', WL_DOMAIN) ?></th>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        $meta_title = '';
                        $meta_keywords = '';
                        $meta_description = '';
                        if ($campaign_template) {
                            $meta_title = $campaign_template->meta_title;
                            $meta_keywords = $campaign_template->meta_keywords;
                            $meta_description = $campaign_template->meta_description;
                        }
                        ?>
                        <table style="width: 60%;margin-left: 10%">
                            <tr>
                                <th>Meta Title</th>
                                <td><input type="text" name="meta_title" value="<?php echo $meta_title ?>" /></td>
                            </tr>
                            <tr>
                                <th>Meta Keywords</th>
                                <td><input type="text" name="meta_keywords" value="<?php echo $meta_keywords ?>" /></td>
                            </tr>
                            <tr>
                                <th>Meta Description</th>
                                <td><textarea rows="3" cols="20" name="meta_description"><?php echo $meta_description ?></textarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th scope="row" colspan="2"><?php _e('List Content Publishing', WL_DOMAIN) ?></th>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        //Used Values
                        {
                            $publish_units = array(
                                'min' => __('Minute', WL_DOMAIN),
                                'hour' => __('Hour', WL_DOMAIN),
                                'day' => __('Day', WL_DOMAIN),
                                'week' => __('Week', WL_DOMAIN),
                                'month' => __('Month', WL_DOMAIN),
                                'year' => __('Year', WL_DOMAIN)
                            );
                            $post_statuses = array(
                                'publish' => __('Published', WL_DOMAIN),
                                'draft' => __('Draft', WL_DOMAIN)
                            );
                        }

                        //default values
                        {
                            $first_publish_value = 0;
                            $first_publish_unit = 'hour';
                            $post_status = 1;
                            $auto_publish_value = 0;
                            $auto_publish_unit = 'hour';
                            $random_times = 0;
                        }

                        //check for saved values
                        if ($campaign_template) {
                            $first_publish_value = $campaign_template->first_publish_value;
                            $first_publish_unit = $campaign_template->first_publish_unit;
                            $post_status = $campaign_template->post_status;
                            $auto_publish_value = $campaign_template->auto_publish_value;
                            $auto_publish_unit = $campaign_template->auto_publish_unit;
                            $random_times = $campaign_template->random_times;
                        }
                        ?>
                        <table style="width: 60%;margin-left: 10%">
                            <tr>
                                <th>Publish First Post in</th>
                                <td>
                                    <input type="number" name="first_publish_value" value="<?php echo $first_publish_value ?>" style="width: 60px" />
                                    <select name="first_publish_unit">
                                        <?php foreach ($publish_units as $fpu => $fpul) { ?>
                                            <option value="<?php echo $fpu ?>"<?php echo $first_publish_unit === $fpu ? ' selected' : '' ?>><?php echo $fpul ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>New Post Status</th>
                                <td>
                                    <select name="post_status">
                                        <?php foreach ($post_statuses as $ps => $psl) { ?>
                                            <option value="<?php echo $ps ?>"<?php echo $post_status === $ps ? ' selected' : '' ?>><?php echo $psl ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Automatically Publish every</th>
                                <td>
                                    <input type="number" name="auto_publish_value" value="<?php echo $auto_publish_value ?>" style="width: 60px" />
                                    <select name="auto_publish_unit">
                                        <?php foreach ($publish_units as $apu => $apul) { ?>
                                            <option value="<?php echo $apu ?>"<?php echo $auto_publish_unit === $apu ? ' selected' : '' ?>><?php echo $apul ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    <label>
                                        <input type="hidden" name="random_times" value="0" />
                                        <input type="checkbox" name="random_times" value="1"<?php echo $random_times === '1' ? ' checked' : '' ?> />
                                        Randomize Publishing Time
                                    </label>
                                </th>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="sub" id="sub" class="button button-primary" value="<?php _e('Save Campaign', WL_DOMAIN) ?>"></p></form>

</div>
<script>
    CKEDITOR.replace('post_prefix');
    CKEDITOR.replace('post_postfix');
    var ajax_url = '<?php echo admin_url('admin-ajax.php') ?>';
    (function ($) {
        $(document)
                .ready(function () {
                    if ($('#filters_div').data('init_filters') == '1') {
                        add_filter();
                    }
                    load_categories();
                    if ($('#content_table_container').data('init_show_table') == '1') {
                        show_content_table();
                    }
                    load_add_categories(true);
                })
                .on('change', '.category_select', function () {
                    var el = $(this);
                    var parent = el.val();
                    $('#category_id').val(parent);
                    el.closest('.load_categories').nextAll('.load_categories').remove();
                    if (parent) {
                        load_categories(parent);
                    } else {
                        $('.category_select').each(function () {
                            if (+$(this).val()) {
                                $('#category_id').val(+$(this).val());
                            }
                        });
                    }
                })
                .on('click', '#add_filter', add_filter)
                .on('change', '#content_table_columns', show_content_table)
                .on('click', '#wl_clear_add_category', function (e) {
                    e.preventDefault();
                    if (confirm('This will clear all the categories and sub-categories selected\nAre you sure?'))
                        load_add_categories();
                })
                .on('click', '#wl_add_new_category', function (e) {
                    e.preventDefault();
                    var el = $(this);
                    el.prop('disabled', true).remove();
                    var container = $('#add_categories_container');
                    container.append('<span class="remove_it">Please Wait...</span>');
                    $.get(ajax_url, {
                        action: 'wl_get_intial_categories',
                    }, function (data) {
                        container.find('.remove_it').remove();
                        container.append(data);
                    });
                })
                .on('click', '#wl_add_sub_category', function (e) {
                    e.preventDefault();
                    var el = $(this);
                    el.prop('disabled', true);
                    var container = $('#wl_subcategories_container');
                    var sub_cat_count = container.find('.wl_subcategory_container').length;
                    container.append('<div class="remove_it">Please Wait...</div>');
                    $.get(ajax_url, {
                        action: 'wl_get_subcategory',
                        sub_cat_count: sub_cat_count
                    }, function (data) {
                        el.prop('disabled', false);
                        container.find('.remove_it').remove();
                        container.append(data);
                        sub_cat_count = container.find('.wl_subcategory_container').length;
                        if (sub_cat_count > 1) {
                            el.parent().remove();
                        }
                    });
                })
                ;
        function show_content_table() {
            var cols = +$('#content_table_columns').val();
            if (cols <= 0) {
                $('#content_table_columns').val(1).change();
                return;
            }
            if (cols > 15) {
                $('#content_table_columns').val(15).change();
                return;
            }
            var content_table_container = $('#content_table_container');
            if (!cols || isNaN(cols)) {
                if (!cols)
                    cols = 0;
                content_table_container.html('<span style="color: red">Invalid Number of Columns (' + cols + ')</span>');
            } else {
                content_table_container.html("Making Table, Please wait...");
                $.get(ajax_url, {
                    action: 'wl_show_table',
                    cols: cols,
                }, function (data) {
                    content_table_container.html(data);
                });
            }
        }
        function add_filter() {
            $.post(ajax_url, {
                action: 'wl_add_filter',
                id: $('#filters_div div[data-for="filter"]').length
            }, function (data) {
                $('#filters_div').append(data).find('div:last-child').hide().fadeIn().find('input:first-child').focus();
            });
        }
        function load_categories(parent) {
            if (!parent || parent === undefined)
                parent = 0;
            $.post(ajax_url, {
                action: 'wl_get_categories',
                parent: parent
            }, function (data) {
                data = $.parseJSON(data);
                if (data.status === 'success') {
                    data = data.data;
                    var load_categories_container = $('#load_categories_container');
                    if (load_categories_container.find('.load_categories[data-parent="' + parent + '"]').length === 0) {
                        load_categories_container.append('<div class="load_categories" style="margin-top: 10px" data-parent="' + parent + '"></div>');
                    }
                    $('.load_categories[data-parent="' + parent + '"]').html(data).hide().fadeIn().find('.category_select').focus();
                }
            });
        }
        function load_add_categories(force) {
            if (force !== true)
                force = false;
            var container = $('#add_categories_container');
            container.html('Loading...');
            var campaign_template_id = 0;
            if ($('#campaign_template_id').length) {
                campaign_template_id = $('#campaign_template_id').val();
            }
            var campaign_id = 0;
            if ($('#campaign_id').length) {
                campaign_id = $('#campaign_id').val();
            }
            $.get(ajax_url, {
                action: 'wl_get_add_categories',
                campaign_id: campaign_id,
                campaign_template_id: campaign_template_id,
                force: force
            }, function (data) {
                container.html(data);
            });
        }
        function sync_call(url, data, post_call, original_response) {
            post_call = typeof post_call === 'boolean' ? post_call : false;
            original_response = typeof original_response !== 'undefined' ? original_response : false;
            $.ajaxSetup({
                async: false
            });
            if (post_call === true) {
                if (original_response === true)
                    return $.post(url, data).responseText;
                return $.parseJSON($.post(url, data).responseText);
            }
            if (original_response === true)
                return $.get(url, data).responseText;
            return $.parseJSON($.get(url, data).responseText);
        }
    })(jQuery);
    function selectText(containerid) {
        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select();
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
        }
    }
</script>