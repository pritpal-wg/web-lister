<?php
$approved_actions = array(
    'del_campaign',
    'del_campaign_posts',
);
$action = isset($_GET['action']) ? $_GET['action'] : '';
$id = (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) ? $_GET['id'] : 0;
$approved = $id && is_string($action) && in_array($action, $approved_actions);
?>
<?php if ($approved) { ?>
    <?php
    $campaign = wl_get_campaign($id);
    $redirect = admin_url("admin.php?page=weblister");
    if (!$campaign) {
        echo '<script>window.location = "' . $redirect . '";</script>';
        die;
    }
    ?>
    <?php
    if (isset($_POST['submit'])) {
        $act = $_POST['action'];
        if (function_exists("wl_$act")) {
            call_user_func("wl_$act", $id);
        }
        echo '<script>window.location = "' . $redirect . '";</script>';
        die;
    }
    ?>
    <div class="wrap">
        <form method="post">
            <input type="hidden" name="action" value="<?php echo $action ?>" />
            <input type="hidden" name="id" value="<?php echo $id ?>" />
            <?php if ($action === 'del_campaign') { ?>
                <h1>Delete Campaign</h1>
            <?php } elseif ($action === 'del_campaign_posts') { ?>
                <h1>Delete Campaign Posts</h1>
            <?php } ?>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">Campaign Name</th>
                        <td>
                            <input type="text" class="regular-text" value="<?php echo $campaign->title ?>" readonly />
                            <?php if ($action === 'del_campaign') { ?>
                                <p class="description">
                                    This will delete all the entire campaign <b><?php echo $campaign->title ?></b>.
                                    <br />
                                    <br />
                                    Campaign name, campaign settings, CSV file and all posts created by this campaign will be deleted.
                                    <br />
                                    <br />
                                    Please note that once campaign is deleted, this cannot be recovered!
                                </p>
                            <?php } elseif ($action === 'del_campaign_posts') { ?>
                                <p class="description">
                                    This will Delete all posts created by Campaign: <b><?php echo $campaign->title ?></b>.
                                    <br />
                                    <br />
                                    The Campaign CSV file and settings will be preserved for a future import.
                                    <br />
                                    <br />
                                    Please note that once posts are deleted, they cannot be recovered!
                                </p>
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <a href="<?php echo admin_url('admin.php?page=weblister') ?>" class="button button-default">Cancel</a> |
                <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e((($action === 'del_campaign') ? 'Delete Campaign' : 'Delete Posts'), WL_DOMAIN) ?>" />
            </p>
        </form>
    </div>
<?php } else { ?>
    <?php $campaigns = wl_get_campaign('', false) ?>
    <div class="wrap">
        <h1>
            Campaigns List
            <a href="<?php echo untrailingslashit(get_site_url()) . "/wp-admin/admin.php?page=weblister_campaign" ?>" class="page-title-action">Add New Campaign</a>
        </h1>
        <?php if (!empty($campaigns)) { ?>
            <hr />
            <div style="max-width: 100%;overflow: auto">
                <table class="widefat fixed" cellspacing="0">
                    <thead>
                        <tr>
                            <th id="campaign" class="manage-column column-campaign" scope="col" style="width: 390px">Campaign</th>
                            <th id="lines_imported" class="manage-column column-lines_imported num" scope="col" style="width: 100px">Lines Imported</th>
                            <th id="articles" class="manage-column column-articles num" scope="col" style="width: 100px">Articles</th>
                            <th id="published" class="manage-column column-published num" scope="col" style="width: 100px">Published</th>
                            <th id="scheduled" class="manage-column column-scheduled num" scope="col" style="width: 100px">Scheduled</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($campaigns as $i => $campaign_info) { ?>
                            <?php
                            $campaign_id = $campaign_info->id;
                            //get number of published posts for this campaign
                            $campaign_info->tot_articles = wl_get_num_articles($campaign_id);
                            $campaign_info->published_articles = wl_get_num_articles($campaign_id, true);
                            $campaign_info->scheduled_articles = $campaign_info->tot_articles - $campaign_info->published_articles;
                            ?>
                            <tr<?php echo ($i % 2) === 0 ? ' class="alternate"' : '' ?>>
                                <td class="column-campaign">
                                    <?php echo $campaign_info->title ?>
                                    <div class="row-actions">
                                        <span><a href="<?php echo trailingslashit(get_admin_url()) . "admin.php?page=weblister_campaign_settings&id=$campaign_id" ?>">Edit</a> |</span>
                                        <span><a href="javascript:;">Copy</a> |</span>
                                        <span><a href="<?php echo trailingslashit(get_admin_url()) . "admin.php?page=weblister&action=del_campaign&id=$campaign_id" ?>">Delete Campaign</a> |</span>
                                        <span><a href="<?php echo trailingslashit(get_admin_url()) . "admin.php?page=weblister&action=del_campaign_posts&id=$campaign_id" ?>">Delete Posts</a> |</span>
                                        <span><a href="<?php echo trailingslashit(get_admin_url()) . "admin.php?page=weblister_campaign&action=import_csv&id=$campaign_id" ?>">Import New CSV</a></span>
                                    </div>
                                </td>
                                <td class="column-lines_imported num"><?php echo $campaign_info->tot_lines ?></td>
                                <td class="column-articles num"><?php echo $campaign_info->tot_articles ?></td>
                                <td class="column-published num"><?php echo $campaign_info->published_articles ?></td>
                                <td class="column-scheduled num"><?php echo $campaign_info->scheduled_articles ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="manage-column column-campaign" scope="col">Campaign</th>
                            <th class="manage-column column-lines_imported num" scope="col">Lines Imported</th>
                            <th class="manage-column column-articles num" scope="col">Articles</th>
                            <th class="manage-column column-published num" scope="col">Published</th>
                            <th class="manage-column column-scheduled num" scope="col">Scheduled</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php } else { ?>
            <div class="notice-error error" style="padding: 10px">
                <strong><?php _e('Sorry', WL_DOMAIN) ?>:</strong>
                <?php _e('There are no Campaigns to display right now. Please add a Campaign first...', WL_DOMAIN) ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>