<div class="wrap">
    <h1>Web Lister Settings</h1>
    <form method="post" action="options.php">
        <?php
        settings_fields(WL_SETTINGS_GROUP);
        do_settings_sections(WL_SETTINGS_GROUP);
        ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Max Import Lines</th>
                <td>
                    <input type="number" min="1" name="wl_import_lines" value="<?php echo esc_attr(get_option('wl_import_lines')); ?>" />
                    <p class="description">
                        Maximum number of lines that are imported at a time
                    </p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Max Post Rows</th>
                <td>
                    <input type="text" name="wl_import_post_records" value="<?php echo esc_attr(get_option('wl_import_post_records')); ?>" />
                    <p class="description">
                        Maximum number of Rows that will be added to one Post during Import.
                    </p>
                </td>
            </tr>
        </table>
        <?php submit_button() ?>
    </form>
</div>