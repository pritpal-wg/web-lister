<?php
$allowed_actions = array(
    'import_csv',
    'create'
);
$action = isset($_GET['action']) && $_GET['action'] ? $_GET['action'] : '';
$id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0 ? $_GET['id'] : 0;
$allowed = is_string($action) && $action && in_array($action, $allowed_actions);
if (!$allowed) {
    $redirect = trailingslashit(get_site_url()) . "wp-admin/admin.php?page=weblister_campaign&action=create";
    echo '<script>window.location = "' . $redirect . '";</script>';
    die;
}
if ($action === 'import_csv' && !$id) {
    $redirect = trailingslashit(get_site_url()) . "wp-admin/admin.php?page=weblister";
    echo '<script>window.location = "' . $redirect . '";</script>';
    die;
}
if (isset($_POST['submit'])) {
//    pr($_POST);
    $error = false;
    $error_desc = array();
    $campaign_name = esc_sql(trim($_POST['campaign_name']));
    $upload_option = $_POST['upload_option'];
    if (!$campaign_name) {
        $error = true;
        $error_desc[] = "Invalid Campaign Name";
    }
    if (!$error) {
        $campaign = array();
        if ($action === 'create') {
            $campaign['title'] = $campaign_name;
            $campaign['date_added'] = date('Y-m-d H:i:s');
        } else {
            $campaign['upload_file_name'] = '';
            $campaign['import_file_url'] = '';
            $campaign['reimport_value'] = '';
            $campaign['reimport_unit'] = '';
        }
        if (isset($_POST["del_old_posts_{$upload_option}"])) {
            $campaign['delete_old_posts'] = $_POST["del_old_posts_{$upload_option}"];
        }
        if ($upload_option === 'upload') {
            $upload_file = $_FILES['upload_file'];
            if ($upload_file['error'] === 4) {
                $error = TRUE;
                $error_desc[] = "Please select a file to upload";
            } elseif ($upload_file['error'] === 0) {
                $file_name = $upload_file['name'];
                $pathinfo = pathinfo($file_name);
                $ext = $pathinfo['extension'];
                if ($ext === 'csv') {
                    $upload_file_name = uniqid($pathinfo['filename'] . "_", true) . ".csv";
                    $file_path = WL_UPLOAD_DIR . $upload_file_name;
                    if (@move_uploaded_file($upload_file['tmp_name'], $file_path)) {
                        $campaign['upload_file_name'] = $upload_file_name;
                        $campaign['csv_file_name'] = $file_name;
                    } else {
                        $error = TRUE;
                        $error_desc[] = "Unable to save uploaded file";
                    }
                } else {
                    $error = TRUE;
                    $error_desc[] = "Wrong File Selected, Only \".csv\" files are allowed.";
                }
            } else {
                $error = true;
                $error_desc[] = "Something Went Wrong";
            }
        } elseif ($upload_option === 'url') {
            $import_url = $_POST['import_url'];
            if (filter_var($import_url, FILTER_VALIDATE_URL) === FALSE) {
                $error = true;
                $error_desc[] = "Invalid URL Provided";
            } else {
                $pathinfo = pathinfo($import_url);
                $ext = $pathinfo['extension'];
                if ($ext === 'csv') {
                    $campaign['import_file_url'] = $import_url;
                    $campaign['csv_file_name'] = $pathinfo['basename'];
                    $campaign['reimport_value'] = $_POST['reimport_value'];
                    $campaign['reimport_unit'] = $_POST['reimport_unit'];
                    //save file to server
                    {
                        $upload_file_name = uniqid($pathinfo['filename'] . "_", true) . ".csv";
                        $import_local_path = WL_UPLOAD_DIR . $upload_file_name;
                        $file_content = file_get_contents($import_url);
                        $handle = fopen($import_local_path, 'w');
                        fwrite($handle, $file_content);
                        fclose($handle);
                        $campaign['upload_file_name'] = $upload_file_name;
                    }
                } else {
                    $error = TRUE;
                    $error_desc[] = "Wrong File in URL, Only \".csv\" files are allowed.";
                }
            }
        } else {
            $error = true;
            $error_desc[] = "Wrong Import Option Selected";
        }
    }

    //everything is ok, we can save the campaign to db
    if (!$error) {
        global $wpdb;
        $success = false;
        if ($action === 'create') {
            $wpdb->insert(WL_TABLE_CAMPAIGNS, $campaign);
            $campaign_id = $wpdb->insert_id;
            $redirect = trailingslashit(get_admin_url()) . "admin.php?page=weblister_campaign_settings&id=$campaign_id";
            echo '<script>window.location = "' . $redirect . '";</script>';
            die;
        } else {
            //delete old schedule for new csv file
            wl_del_campaign_schedule($id);
            $wpdb->update(WL_TABLE_CAMPAIGNS, $campaign, array('id' => $id));
            $redirect = admin_url("admin.php?page=weblister_campaign_settings&id=$id");
            echo '<script>window.location = "' . $redirect . '";</script>';
            die;
        }
    }
}
?>
<div class="wrap">
    <h1><?php _e(($action === 'import_csv' ? 'Import New CSV' : 'Add New Campaign'), WL_DOMAIN) ?></h1>
    <?php if (isset($error) && $error === TRUE) { ?>
        <div class="error" style="padding-top: 5px">
            <strong>Error(s) Occoured:</strong>
            <hr />
            <ul>
                <?php foreach ($error_desc as $err) { ?>
                    <li><?php echo $err ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <form method="POST" enctype="multipart/form-data">
        <input type="hidden" name="action" value="<?php echo $action ?>" />
        <?php if ($id) { ?>
            <input type="hidden" name="id" value="<?php echo $id ?>" />
        <?php } ?>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="campaign_name"><?php _e('Campaign Name', WL_DOMAIN) ?></label>
                    </th>
                    <td>
                        <input name="campaign_name" type="text" id="campaign_name" value="<?php echo $action === 'import_csv' ? 'Campaign Name' : '' ?>" class="regular-text" required<?php echo $action === 'import_csv' ? ' readonly' : '' ?> />
                        <p class="description"><?php _e('The Name of the CSV import campaign', WL_DOMAIN) ?>.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e('Import Options', WL_DOMAIN) ?></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span><?php _e('Import Options', WL_DOMAIN) ?></span></legend>
                            <label title="Upload"><input type="radio" name="upload_option" value="upload" checked> Upload</label><br />
                            <input type="file" name="upload_file" class="regular-text" /><br />
                            <label>
                                <input type="hidden" name="del_old_posts_upload" value="0" />
                                <input type="checkbox" value="1" name="del_old_posts_upload" />
                                <font color="red"><?php _e('Delete', WL_DOMAIN) ?></font> <?php _e('old posts if not found in the update file', WL_DOMAIN) ?>.
                            </label>
                            <hr />
                            <label title="URL"><input type="radio" name="upload_option" value="url" /> URL</label><br />
                            <input type="url" name="import_url" class="regular-text" placeholder="e.g. http://www.example.com/xyz.csv" />
                            <br />
                            <?php _e('Re-Import Every', WL_DOMAIN) ?>
                            <input type="number" name="reimport_value" min="0" style="width: 60px" />
                            <select name="reimport_unit">
                                <option value=""><?php _e('Never', WL_DOMAIN) ?></option>
                                <option value="min"><?php _e('Minute', WL_DOMAIN) ?></option>
                                <option value="hour"><?php _e('Hour', WL_DOMAIN) ?></option>
                                <option value="day"><?php _e('Day', WL_DOMAIN) ?></option>
                                <option value="week"><?php _e('Week', WL_DOMAIN) ?></option>
                                <option value="month"><?php _e('Month', WL_DOMAIN) ?></option>
                                <option value="year"><?php _e('Year', WL_DOMAIN) ?></option>
                            </select>
                            <br />
                            <label>
                                <input type="hidden" name="del_old_posts_url" value="0" />
                                <input type="checkbox" value="1" name="del_old_posts_url" />
                                <font color="red"><?php _e('Delete', WL_DOMAIN) ?></font> <?php _e('old posts if not found in the update file', WL_DOMAIN) ?>.
                            </label>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Done', WL_DOMAIN) ?>"></p>
    </form>
</div>