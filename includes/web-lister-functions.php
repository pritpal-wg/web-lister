<?php
if (!function_exists('pr')) {

    function pr($e) {
        echo '<pre>';
        print_r($e);
        echo '</pre>';
    }

}

if (!function_exists('vd')) {

    function vd($e, $plain_text = FALSE) {
        if ($plain_text === TRUE)
            @header("Content-Type: text/plain", TRUE);
        echo $plain_text !== TRUE ? '<pre>' : '';
        var_dump($e);
        echo $plain_text !== TRUE ? '</pre>' : '';
    }

}

function wl_get_categories() {
    extract($_POST);
    $categories = get_categories(
            array(
                'parent' => $parent,
                'hide_empty' => 0,
            )
    );
    $ret = array();
    $ret['status'] = 'error';
    if ($categories) {
        $ret['status'] = 'success';
        ob_start();
        ?>
        <select class="category_select">
            <option value="">Select Category</option>
            <?php foreach ($categories as $category_info) { ?>
                <option value="<?php echo $category_info->term_id ?>"><?php echo $category_info->name ?></option>
            <?php } ?>
        </select>
        <?php
        $data = ob_get_clean();
        $ret['data'] = $data;
    }
    echo json_encode($ret);
    die;
}

function wl_add_filter() {
    ob_start();
    extract($_POST);
    ?>
    <div style="margin-top: 10px" data-for="filter">
        <input type="text" style="width: 60px" name="filters[<?php echo $id ?>][field]" placeholder="Field" />
        <select name="filters[<?php echo $id ?>][unit]">
            <option value="" hidden>Unit</option>
            <option value="eq">= (<?php _e('equal', WL_DOMAIN) ?>)</option>
            <option value="lg">&gt; (<?php _e('larger', WL_DOMAIN) ?>)</option>
            <option value="sm">&lt; (<?php _e('smaller', WL_DOMAIN) ?>)</option>
            <option value="df">&lt;&gt; (<?php _e('different', WL_DOMAIN) ?>)</option>
        </select>
        <input type="text" style="width: 150px" name="filters[<?php echo $id ?>][value]" placeholder="Value" />
    </div>
    <?php
    echo ob_get_clean();
    die;
}

function wl_file_ext($filename) {
    $ext = null;
    if (is_array($filename) && array_key_exists('name', $filename))
        $filename = $filename['name'];
    if (!is_string($filename) || strpos($filename, '.') === FALSE)
        return $ext;
    $ext = end(explode('.', $filename));
    return $ext;
}

function wl_get_campaign($id = null, $active = TRUE) {
    global $wpdb;
    $select_clause = "SELECT * FROM `" . WL_TABLE_CAMPAIGNS . "`";
    $where_clause = "1=1";
    $get_single = FALSE;
    if (is_numeric($id) && $id > 0) {
        $where_clause .= " AND id=$id";
        $get_single = TRUE;
    }
    if (!(is_numeric($id) && $id > 0) && $active === TRUE) {
        $where_clause .= " AND `is_active`='1'";
    }
    $query = $select_clause . " WHERE $where_clause";
    return $get_single ? $wpdb->get_row($query) : $wpdb->get_results($query);
}

function wl_del_campaign($id) {
    global $wpdb;
    $ret = FALSE;
    if (is_numeric($id) && $id > 0 && wl_get_campaign($id)) {
        wl_del_campaign_posts($id);
        wl_del_campaign_schedule($id);
        $ret = $wpdb->delete(WL_TABLE_CAMPAIGNS, array('id' => $id));
    } elseif ($id === -1) {
//        $ret = $wpdb->query("TRUNCATE TABLE `" . WL_TABLE_CAMPAIGNS . "`");
    }
    return $ret;
}

function wl_del_campaign_schedule($campaign_id) {
    global $wpdb;
    $query = "DELETE FROM `" . WL_TABLE_CAMPAIGN_SCHEDULE . "` WHERE campaign_id=$campaign_id";
    return $wpdb->query($query);
}

function wl_del_campaign_posts($campaign_id) {
    $e = new WP_Query(array(
        'post_type' => 'post',
        'post_status' => 'any',
        'posts_per_page' => -1,
        'fields' => 'ids',
        'meta_query' => array(
            array(
                'key' => '_wl_post',
                'value' => 1,
            ),
            array(
                'key' => '_wl_campaign_id',
                'value' => $campaign_id,
            ),
        ),
    ));
    if ($e->have_posts()) {
        //delete posts here
        while ($e->have_posts()) {
            $e->the_post();
            global $post;
            wp_delete_post($post, TRUE);
        }
    }
    wp_reset_query();
}

function wl_show_table() {
    ob_start();
    extract($_GET);
    if (!$cols || $cols < 0)
        $cols = 1;
    if ($cols > 15)
        $cols = 15;
    ?>
    <table border="1" style="width: 100%" id="content_table">
        <thead>
            <tr>
                <?php for ($i = 0; $i < $cols; $i++) { ?>
                    <th><input type="text" placeholder="Heading Text" name="post_content[<?php echo $i + 1 ?>][heading]" style="width: 100%" /></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php for ($i = 0; $i < $cols; $i++) { ?>
                    <td><input type="text" name="post_content[<?php echo $i + 1 ?>][text]" style="width: 100%" placeholder="Content" /></td>
                <?php } ?>
            </tr>
        </tbody>
    </table>
    <style>
        #content_table,
        #content_table tr,
        #content_table tr th,
        #content_table tr td
        {
            padding: 1px;
            margin:0;
        }
    </style>
    <?php
    echo ob_get_clean();
    die;
}

function wl_get_campaign_csv_url($id) {
    if ($campaign = wl_get_campaign($id)) {
        $csv_url = '';
        if ($campaign->upload_file_name) {
            $csv_url = WL_UPLOAD_DIR_URL . $campaign->upload_file_name;
        } elseif ($campaign->import_file_url) {
            $csv_url = $campaign->import_file_url;
        }
        if (wl_valid_csv_url($csv_url))
            return $csv_url;
    }
    return false;
}

function wl_valid_csv_url($url) {
    if ($url = filter_var($url, FILTER_VALIDATE_URL)) {
        $pathinfo = pathinfo($url);
        $ext = $pathinfo['extension'];
        if ($ext === 'csv') {
            return TRUE;
        }
    }
    return FALSE;
}

function wl_get_campaign_template($id) {
    if ($campaign = wl_get_campaign($id)) {
        global $wpdb;
        $query = "SELECT * FROM `" . WL_TABLE_CAMPAIGN_TEMPLATES . "` WHERE campaign_id=$id";
        $campaign_template = $wpdb->get_row($query);
        return $campaign_template;
    }
    return null;
}

function wl_get_csv_line_data($line_var, $get = 'val') {
    $allowed_gets = array(
        'val',
        'col',
        'var'
    );
    if (is_string($get)) {
        $get = trim($get);
    }
    if (in_array($get, $allowed_gets)) {
        if (array_key_exists($get, $line_var)) {
            return $line_var[$get];
        }
    }
    return null;
}

function wl_fill_post_var_vals($input = '') {
    if (!is_string($input))
        return null;
    if (strpos($input, '{wl:') !== FALSE) {
        global $cols_vars, $line;
        $i = 0;
        foreach ($cols_vars as $var) {
            if (strpos($input, $var) !== FALSE) {
                $line_no = $i;
                $input = str_replace($var, wl_get_csv_line_data($line[$line_no]), $input);
            }
            $i++;
        }
    }
    return $input;
}

function wl_get_num_articles($campaign_id, $published = false) {
    $query = new WP_Query(array(
        'post_type' => 'post',
        'post_status' => ($published === true ? 'publish' : 'any'),
        'posts_per_page' => -1,
        'fields' => 'ids',
        'meta_query' => array(
            array(
                'key' => '_wl_post',
                'value' => 1
            ),
            array(
                'key' => '_wl_campaign_id',
                'value' => $campaign_id
            ),
        ),
    ));
    return count($query->posts);
}

function wl_get_add_categories() {
    extract($_GET);
    $use_old = false;
    if ($campaign_template_id) {
        $campaign_template = wl_get_campaign_template($campaign_id);
        $use_old = $campaign_template->use_camp_categories ? true : false;
    }
    ob_start();
    ?>
    <div style="float: right">
        <a id="wl_clear_add_category" href="javascript:;" style="text-decoration: none">Clear All</a>
    </div>
    <?php if ($force === 'true' && $use_old) { ?>
        <?php // pr($campaign_template) ?>
        <div id="wl_parent_category_container">
            <?php
            $args = array(
                'show_option_none' => '-- No Parent Category --',
                'option_none_value' => '',
                'orderby' => 'NAME',
                'hide_empty' => 0,
                'hierarchical' => 1,
                'name' => 'wl_camp_parent_category',
                'value_field' => 'term_id',
                'selected' => $campaign_template->camp_parent_category,
            );
            wp_dropdown_categories($args);
            $camp_categories = unserialize($campaign_template->camp_categories);
            $camp_category = array_shift($camp_categories);
            $num_sub_cats = count($camp_categories);
            ?>
            <br />
            <input type="text" style="margin-top: 5px;width: 24em" name="wl_camp_categories[]" required placeholder="Category Description" value="<?php echo $camp_category ?>" />
        </div>
        <div id="wl_subcategories_container" style="max-height: 100px;overflow: auto;margin-top:5px;">
            <?php if (!empty($camp_categories)) { ?>
                <?php $sub_cat_count = 1; ?>
                <?php foreach ($camp_categories as $camp_category) { ?>
                    <div class="wl_subcategory_container" style="margin-bottom: 5px;">
                        <input type="text" style="width: 24em;margin-left: <?php echo $sub_cat_count++ * 10 ?>px" name="wl_camp_categories[]" placeholder="Category Description" value="<?php echo $camp_category ?>" />
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if ($num_sub_cats < 2) { ?>
            <div style="padding: 10px 2px;">
                <a id="wl_add_sub_category" href="javascript:;" style="text-decoration: none">Add Sub Category</a>
            </div>
        <?php } ?>
    <?php } else { ?>
        <a id="wl_add_new_category" href="javascript:;" style="text-decoration: none">Add New Category</a>
    <?php } ?>
    <?php
    echo ob_get_clean();
    die;
}

function wl_get_intial_categories() {
    ob_start();
    ?>
    <div id="wl_parent_category_container">
        <?php
        $args = array(
            'show_option_none' => '-- No Parent Category --',
            'option_none_value' => '',
            'orderby' => 'NAME',
            'hide_empty' => 0,
            'hierarchical' => 1,
            'name' => 'wl_camp_parent_category',
            'value_field' => 'term_id',
        );
        wp_dropdown_categories($args);
        ?>
        <br />
        <input type="text" style="margin-top: 5px;width: 24em" name="wl_camp_categories[]" required placeholder="Category Description" />
    </div>
    <div id="wl_subcategories_container" style="max-height: 100px;overflow: auto;margin-top:5px;"></div>
    <div style="padding: 10px 2px;">
        <a id="wl_add_sub_category" href="javascript:;" style="text-decoration: none">Add Sub Category</a>
    </div>
    <?php
    echo ob_get_clean();
    die;
}

function wl_get_subcategory() {
    ob_start();
    extract($_GET);
    $sub_cat_count++;
    ?>
    <div class="wl_subcategory_container" style="margin-bottom: 5px;">
        <input type="text" style="width: 24em;margin-left: <?php echo $sub_cat_count * 10 ?>px" name="wl_camp_categories[]" placeholder="Category Description" />
    </div>
    <?php
    echo ob_get_clean();
    die;
}
