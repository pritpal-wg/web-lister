<?php

class WebLister_Activator {

    public static function activate() {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = "CREATE TABLE IF NOT EXISTS `" . WL_TABLE_CAMPAIGNS . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) NOT NULL,
            `tot_lines` bigint(20) NOT NULL,
            `tot_articles` int(11) NOT NULL,
            `published_articles` int(11) NOT NULL,
            `scheduled_articles` int(11) NOT NULL,
            `csv_file_name` varchar(255) NOT NULL,
            `upload_file_name` text NOT NULL,
            `import_file_url` text NOT NULL,
            `delete_old_posts` tinyint(1) NOT NULL DEFAULT '0',
            `reimport_value` int(11) NOT NULL,
            `reimport_unit` varchar(10) NOT NULL,
            `is_active` tinyint(1) NOT NULL DEFAULT '1',
            `date_added` datetime NOT NULL,
            `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `start_datetime` datetime NOT NULL,
            `end_datetime` datetime NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        dbDelta($sql);
        $sql = "CREATE TABLE IF NOT EXISTS `" . WL_TABLE_CAMPAIGN_SCHEDULE . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `campaign_id` int(11) NOT NULL,
            `reimport_datetime` datetime NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        dbDelta($sql);
        $sql = "CREATE TABLE IF NOT EXISTS `" . WL_TABLE_CAMPAIGN_TEMPLATES . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `campaign_id` int(11) NOT NULL,
            `category_id` int(11) NOT NULL,
            `use_camp_categories` tinyint(4) NOT NULL DEFAULT '0',
            `camp_parent_category` int(11) NOT NULL,
            `camp_categories` text NOT NULL,
            `cols_vars` text NOT NULL,
            `filters` text NOT NULL,
            `post_title` varchar(255) NOT NULL,
            `post_content` text NOT NULL,
            `post_prefix` text NOT NULL,
            `post_postfix` text NOT NULL,
            `tags` text NOT NULL,
            `comments` tinyint(1) NOT NULL DEFAULT '0',
            `trackbacks` tinyint(1) NOT NULL DEFAULT '0',
            `meta_title` varchar(255) NOT NULL,
            `meta_keywords` text NOT NULL,
            `meta_description` text NOT NULL,
            `first_publish_value` int(11) NOT NULL,
            `first_publish_unit` varchar(10) NOT NULL,
            `post_status` varchar(10) NOT NULL,
            `auto_publish_value` int(11) NOT NULL,
            `auto_publish_unit` varchar(10) NOT NULL,
            `random_times` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        dbDelta($sql);
    }

}
