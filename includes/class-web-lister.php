<?php

class WebLister {

    protected $loader;
    protected $plugin_name;
    protected $version;

    public function __construct() {

        $this->plugin_name = 'web-lister';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    private function load_dependencies() {

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-web-lister-loader.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-web-lister-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-web-lister-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-web-lister-public.php';
        $this->loader = new WebLister_Loader();
    }

    private function set_locale() {

        $plugin_i18n = new WebLister_i18n();
        $plugin_i18n->set_domain($this->get_plugin_name());

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    private function define_admin_hooks() {

        $plugin_admin = new WebLister_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        //action for adding menu in admin
        $this->loader->add_action('admin_menu', $plugin_admin, 'admin_menu');

        //register the settings for weblister
        $this->loader->add_action('admin_init', $plugin_admin, 'register_settings');

        //active weblister menu for article and article categories pages
        $this->loader->add_action('admin_head', $plugin_admin, 'make_admin_menu_active');

        //admin ajax hooks
        add_action('wp_ajax_wl_get_categories', 'wl_get_categories');
        add_action('wp_ajax_wl_add_filter', 'wl_add_filter');
        add_action('wp_ajax_wl_show_table', 'wl_show_table');
        add_action('wp_ajax_wl_get_add_categories', 'wl_get_add_categories');
        add_action('wp_ajax_wl_get_intial_categories', 'wl_get_intial_categories');
        add_action('wp_ajax_wl_get_subcategory', 'wl_get_subcategory');
    }

    private function define_public_hooks() {

        $plugin_public = new WebLister_Public($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }

}
